package com.app.main;

import com.app.entities.Consumer;
import com.app.entities.Producer;
import java.util.*;

public class Main {

    public static Random random = new Random();     //Initialized a Random Object
    public static List<Integer> cart = new ArrayList<>();   //Initialized an ArrayList
    public static volatile boolean running = true;  //It is used to avoid the use of cache value for the variable running
    public static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Starting ...");
        System.out.println("Press Return to Stop!");
        System.out.println("*********************");
        Producer addProd = new Producer();  //Created a Producer thread
        Consumer remProd = new Consumer();  //Created a Consumer thread
        addProd.start();                    //The thread is in runnable state
        remProd.start();                    //The thread is in runnable state
        scanner.nextLine();                 //For catching a return 
        addProd.shutdown();                 //Stops the thread if an return is encountered
        remProd.shutdown();                 //Stops the thread if an return is encountered
        System.out.println("Main ends");

    }
}
