package com.app.entities;

import static com.app.main.Main.cart;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Consumer extends Thread {  //Extending the Thread class

    private volatile boolean consumer = true;
    public static int temp;
    @Override
    public void run() {                 //The run() method is overridded
        while (consumer) {              //Keeps removing products till consumer variable is true
            removeProduct();            //Calling removeProduct() function
        }
    }

    public void shutdown() {            //This function is meant for stopping the thread
        consumer = false;               //If the producer variable turns false
    }

    public static void removeProduct() {
        synchronized (cart) {           //Synchronized Block for preventing thread interference
            if (cart.size() > 0) {      //Keep removing if there are objects present in the cart

                temp = cart.remove(cart.size() - 1);    //Remove the last element
                System.out.println("The removed object is : " + temp);  //Print the removed object
                cart.notify();                              //Notify the other thread the it can carry on its operation.
                System.out.println("List : " + cart);
                
                while (cart.isEmpty()) {                    //If the cart is empty, 
                    try {
                        cart.wait();                        //Put the thread in wait state.
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Consumer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        }
        


    }
}
