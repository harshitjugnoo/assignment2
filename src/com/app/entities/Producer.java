package com.app.entities;

import static com.app.main.Main.cart;
import static com.app.main.Main.random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Producer extends Thread {

    private volatile boolean producer = true;
    static int value;
    @Override
    public void run() {                         //The run() method is overridded
        while (producer) {                      //Keeps adding products till producer variable is true
            addProduct();                       //Calling addProduct() function
        }
    }

    public void shutdown() {                    //This function is meant for stopping the thread
        producer = false;                       //If the producer variable turns false
    }

    public static void addProduct() {
        synchronized (cart) {                   //Synchronized Block for preventing thread interference
            if (cart.size() < 20) {             //Keep adding if the list is not full

                value = random.nextInt(100);    //Generate a random integer 
                cart.add(value);                //Add the number to the list
                
                cart.notify();                  //Notify the other thread the it can carry on its operation.
                System.out.println("List : " + cart);
                System.out.println("An object is added : " + value);
                
                while (cart.size() == 20) {     //If the cart is full
                    try {
                        cart.wait();            //Ask the thread to wait
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Producer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        }
        



    }
}
